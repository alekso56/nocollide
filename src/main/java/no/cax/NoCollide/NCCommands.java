package no.cax.NoCollide;

import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NCCommands implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if(arg2.equals("nocollide") || arg2.equals("nco")){
			if(arg0.hasPermission("nocollide.op") || arg0.isOp()){
				NoCollide plugin = NoCollide.getPlugin(NoCollide.class);
				if(arg3.length == 1){
					//toggle whitelist, toggle on/off
					if(arg3[0].equals("whitelist")){
						arg0.sendMessage("Toggled whitelist to "+!plugin.config.getBoolean(plugin.whitelistEnabled));
						plugin.config.set(plugin.whitelistEnabled, !plugin.config.getBoolean(plugin.whitelistEnabled));
						plugin.saveConfig();
					}else if(arg3[0].equals("toggleent")){
						arg0.sendMessage("Toggled nocollide entities to "+!plugin.config.getBoolean(plugin.NoCollideAll));
						plugin.config.set(plugin.NoCollideAll, !plugin.config.getBoolean(plugin.NoCollideAll));
						plugin.saveConfig();
						plugin.setTeamOption();
					}else if(arg3[0].equals("list")){
						String list = "";
						Iterator<String> x = plugin.config.getStringList(plugin.whitelist).iterator();
						while(x.hasNext()){
							list = list + "- "+x.next()+"\n";
						}
						arg0.sendMessage(list);
					}else{
						arg0.sendMessage("/nocollide whitelist/rm/add/list/toggleent");
					}
					return true;
				}else if (arg3.length >= 2){
					//add or delete worlds from whitelist
					//command rm/add world
					List<String> list = plugin.config.getStringList(plugin.whitelist);
					if(arg3[0].equals("rm")){
						if(list.isEmpty()){
							arg0.sendMessage("Whitelist is empty, cannot remove!");
						}else if(!list.contains(arg3[1])){
							arg0.sendMessage("That world does not exist in the whitelist :l");
						}else if(list.remove(arg3[1])){
							arg0.sendMessage("Successfully removed "+arg3[1]+" from the whitelist!");
							plugin.config.set(plugin.whitelist, list);
							plugin.saveConfig();
							for(Player x : Bukkit.getOnlinePlayers()){
								NCEvents.instance(x);
							}
						}
					}else if(arg3[0].equals("add")){
						if(list.contains(arg3[1])){
							arg0.sendMessage("World already present in whitelist");
						}else if(list.add(arg3[1])){
							arg0.sendMessage("Added "+arg3[1]+" to the whitelist");
							plugin.config.set(plugin.whitelist, list);
							plugin.saveConfig();
							for(Player x : Bukkit.getOnlinePlayers()){
								NCEvents.instance(x);
							}
						}
					}else{
						arg0.sendMessage("/nocollide rm/add world");
					}
					return true;
				}else{
					arg0.sendMessage("/nocollide help");
					return true;
				}
			}else{
				arg0.sendMessage("permission denied");
				return true;
			}
		}
		return false;
	}

}
