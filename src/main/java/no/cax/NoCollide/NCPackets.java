package no.cax.NoCollide;

import java.util.Arrays;

import org.bukkit.scoreboard.Team.Option;
import org.bukkit.scoreboard.Team.OptionStatus;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerOptions;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;

import no.cax.NoCollide.WrapperPlayServerScoreboardTeam.Mode;

public class NCPackets {
 void init(NoCollide plugin){
			ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(plugin,ListenerPriority.NORMAL,Arrays.asList(PacketType.Play.Server.SCOREBOARD_TEAM),ListenerOptions.ASYNC){
				@Override
				public void onPacketSending(PacketEvent e){
					if(!e.isCancelled()){
						PacketType packetType = e.getPacketType();
						if (packetType.equals(PacketType.Play.Server.SCOREBOARD_TEAM)) {
							WrapperPlayServerScoreboardTeam team = new WrapperPlayServerScoreboardTeam(e.getPacket().deepClone());
							PacketHandler(e,team);
						}
					}
				}
			});
 }
 void PacketHandler(PacketEvent e, WrapperPlayServerScoreboardTeam team){
	    NoCollide plugin = NoCollide.getPlugin(NoCollide.class);
		if (team.getMode() == Mode.TEAM_CREATED || team.getMode() == Mode.TEAM_UPDATED) {
				if(!NCEvents.shouldCollide(plugin, e.getPlayer())){
					 team.setCollisionRule(plugin.NoCollisionTeam.getOption(Option.COLLISION_RULE).equals(OptionStatus.NEVER)?"never":"pushOtherTeams");
					 e.setPacket(team.getHandle());
					//this.getLogger().info("Disabled collision for "+e.getPlayer().getName()+" owo");
				}
		}else if (team.getMode() == Mode.TEAM_REMOVED){
				if(!NCEvents.shouldCollide(plugin, e.getPlayer())){
					e.setCancelled(true);
					return;
				}
		}
	}
}
