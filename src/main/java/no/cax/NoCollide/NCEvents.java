package no.cax.NoCollide;


import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class NCEvents implements Listener{
	@EventHandler
	public void onworldchange(PlayerChangedWorldEvent event){
		instance(event.getPlayer());
	}
	@EventHandler
	public void onLogin(PlayerJoinEvent event){
		instance(event.getPlayer());
	}
	
	static void instance(Player player) {
		NoCollide plugin = NoCollide.getPlugin(NoCollide.class);
		if(shouldCollide(plugin, player)){
			player.setCollidable(true);
			if(plugin.getConfig().getBoolean(plugin.UsingTeams)){
				if(!plugin.NoCollisionTeam.hasEntry(player.getName())){
					plugin.NoCollisionTeam.removeEntry(player.getName());
					//plugin.getLogger().log(Level.INFO, "added "+player.getName()+" to nocollision");
				}
				player.setScoreboard(plugin.scoreboardCollision);
			}
		}else{
			player.setCollidable(false);
			if(plugin.getConfig().getBoolean(plugin.UsingTeams)){
				if(!plugin.NoCollisionTeam.hasEntry(player.getName())){
					plugin.NoCollisionTeam.addEntry(player.getName());
					//plugin.getLogger().log(Level.INFO, "added "+player.getName()+" to nocollision");
				}
				player.setScoreboard(plugin.scoreboardCollision);
			}
		}
	}
	@EventHandler
	public void onLogout(PlayerQuitEvent event){
		NoCollide plugin = NoCollide.getPlugin(NoCollide.class);
		if(plugin.getConfig().getBoolean(plugin.UsingTeams)){
			if(plugin.NoCollisionTeam.hasEntry(event.getPlayer().getName())){
				plugin.NoCollisionTeam.removeEntry(event.getPlayer().getName());
				//plugin.getLogger().log(Level.INFO, "removed "+player.getName()+" from nocollision");
			}
		}
	}

	static Boolean shouldCollide(NoCollide plugin,Player player){
		if(plugin.getConfig().getBoolean(plugin.whitelistEnabled)){
			if(plugin.getConfig().getStringList(plugin.whitelist).contains(player.getWorld().getName())){
				return false;
			}else{
				return true;
			}
		}
		return false;
	}
}
