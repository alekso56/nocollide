package no.cax.NoCollide;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bukkit.scoreboard.Team.Option;
import org.bukkit.scoreboard.Team.OptionStatus;


public class NoCollide extends JavaPlugin{
	FileConfiguration config = getConfig();
	final String whitelist = "World-Whitelist";
	final String whitelistEnabled = "UseWhiteList";
	final String version = "Version";
	final String UsingTeams = "UsingTeams";
	final String NoCollideAll = "NoCollideAllEntities";
	final String UsingProtocolLib = "usingProtocolLib";
	Team NoCollisionTeam;
	Scoreboard scoreboardCollision;
	//static boolean worldguard;
	
	@Override
	public void onEnable() {
		if(!config.isSet("UseWhiteList")){
			String[] array = {"world","world_nether","world_the_end"};
			config.addDefault(whitelist, Arrays.asList(array));
			config.addDefault(whitelistEnabled, false);
			config.addDefault(UsingTeams, true);
			config.addDefault(UsingProtocolLib, true);
			config.addDefault(NoCollideAll, false);
			config.addDefault(version, this.getDescription().getVersion());
			config.options().copyDefaults(true);
			saveConfig();
		}
		if(config.isSet("Enabled")){
			config.set("Enabled", null);
			config.set(UsingProtocolLib, true);
			config.set(version, this.getDescription().getVersion());
			saveConfig();
		}
		this.scoreboardCollision = Bukkit.getScoreboardManager().getMainScoreboard();
		this.NoCollisionTeam = getOrCreateTeam("NoCollision");
		setTeamOption();
		this.getCommand("nocollide").setExecutor(new NCCommands());
		this.getCommand("nco").setExecutor(new NCCommands());
		getServer().getPluginManager().registerEvents(new NCEvents(), this);
		if(config.getBoolean(UsingProtocolLib)){
			Plugin ProtocolLib = this.getServer().getPluginManager().getPlugin("ProtocolLib");
			if(ProtocolLib != null && ProtocolLib.isEnabled()){
			 new NCPackets().init(this);
			 this.getLogger().info("Successfully hooked into Protocollib!");
			}
		}
		/*Plugin WorldGuard = getServer().getPluginManager().getPlugin("WorldGuard");
		if(WorldGuard != null && WorldGuard.isEnabled()){
			worldguard = true;
			WorldGuardPlugin WG = (WorldGuardPlugin) WorldGuard;
		}else{
			worldguard = false;
		}*/
	}

	@Override
	public void onDisable() {
		this.saveConfig();
		this.setEnabled(false);
	}

	public Boolean shouldCollide(Player player) {
		return NCEvents.shouldCollide(NoCollide.getPlugin(NoCollide.class),player);
	}
	public OptionStatus getOptionStatus() {
		return NoCollide.getPlugin(NoCollide.class).NoCollisionTeam.getOption(Option.COLLISION_RULE);
	}
	
	void setTeamOption(){
		this.NoCollisionTeam.setOption(Option.COLLISION_RULE, getConfig().getBoolean(NoCollideAll)?OptionStatus.NEVER:OptionStatus.FOR_OWN_TEAM);
	}

	Team getOrCreateTeam(String name) {
		Team team = scoreboardCollision.getTeam(name);
		if (team == null) {
			team = scoreboardCollision.registerNewTeam(name);
		}
		return team;
	}
}
